<footer class="site-footer" style="background-image: url(images/big_image_3.jpg);">
  <div class="container">
    <div class="row mb-5">
      <div class="col-md-4">
        <h3>About</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nisi, accusantium optio unde perferendis eum illum voluptatibus dolore tempora, consequatur minus asperiores temporibus reprehenderit.</p>
      </div>
      <div class="col-md-6 ml-auto">
        <div class="row">
          <div class="col-md-4">
            <ul class="list-unstyled">
              <li><a href="#">About Us</a></li>
              <li><a href="#">Company</a></li>
              <li><a href="#">Teachers</a></li>
              <li><a href="#">Courses</a></li>
              <li><a href="#">Categories</a></li>
            </ul>
          </div>
          <div class="col-md-4">
            <ul class="list-unstyled">
              <li><a href="#">About Us</a></li>
              <li><a href="#">Company</a></li>
              <li><a href="#">Teachers</a></li>
              <li><a href="#">Courses</a></li>
              <li><a href="#">Categories</a></li>
            </ul>
          </div>
          <div class="col-md-4">
            <ul class="list-unstyled">
              <li><a href="#">About Us</a></li>
              <li><a href="#">Company</a></li>
              <li><a href="#">Teachers</a></li>
              <li><a href="#">Courses</a></li>
              <li><a href="#">Categories</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
      </div>
    </div>
  </div>
</footer>
<!-- END footer -->

<!-- loader -->
<div id="loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#f4b214"/></svg></div>

<script src="assets/js/jquery-3.2.1.min.js"></script>
<script src="assets/js/jquery-migrate-3.0.0.js"></script>
<script src="assets/js/popper.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/owl.carousel.min.js"></script>
<script src="assets/js/jquery.waypoints.min.js"></script>
<script src="assets/js/jquery.stellar.min.js"></script>


<script src="assets/js/main.js"></script>
</body>
</html>
